class Joueur{

	constructor(nom,id,u1,u2,u3){
		this.nom = nom;
		this.id = id;
		this.pv = 1000;
		this.po = 200;
		this.nbUnites = 3;
		this.u1 = u1;
		this.u2 = u2;
		this.u3 = u3;
	}

	recupererUnite(idUnit){
		if(idUnit == this.u3.id) { this.u3 = new Unit(this.u3.id,"self"); }
		if(idUnit == this.u2.id) { this.u2 = new Unit(this.u2.id,"self"); }
		if(idUnit == this.u1.id) { this.u1 = new Unit(this.u1.id,"self"); }
		this.nbUnites++;
	}

	enleverUnite(){
		this.nbUnites--;
	}

	enleverTous(){
		this.nbUnites = 0;
	}

	getNextUnit(){

		if(this.u3.emplacement == "self"){ return this.u3; }
		if(this.u2.emplacement == "self"){ return this.u2; }
		if(this.u1.emplacement == "self"){ return this.u1; }
		return -1;
	}


	changeNextUnit(emplacement){
		if(this.u3.emplacement == "self"){ this.u3 = new Unit(this.u3.id,emplacement); return; }
		if(this.u2.emplacement == "self"){ this.u2 = new Unit(this.u2.id,emplacement); return; }
		if(this.u1.emplacement == "self"){ this.u1 = new Unit(this.u1.id,emplacement); return; }		
	}

	set setPv(pv){
		this.pv = pv;
	}

	get getPv(){
		return this.pv;
	}

	set setPo(po){
		this.po = po;
	}

	get getPo(){
		return this.po;
	}

	set setId(id){
		this.id = id;
	}


	get getId(){
		return this.id;
	}











}